<?php

namespace Drupal\Tests\purge_control\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests PurgeControl service class.
 *
 * @coversDefaultClass \Drupal\purge_control\Services\PurgeControl
 *
 * @group purge_control
 */
class PurgeControlTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['purge_control'];

  /**
   * Purge control service instance.
   *
   * @var \Drupal\purge_control\Services\PurgeControl
   */
  protected $purgeControl;

  /**
   * Purge settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $purgeSettings;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(['purge_control']);
    $this->purgeControl = $this->container->get('purge_control.purge_control');
    $this->purgeSettings = $this->config('purge_control.settings');
  }

  /**
   * Test enabling/disabling of the purge.
   */
  public function testEnableDisablePurge() {
    // By default purge is enabled.
    $this->assertFalse($this->purgeSettings->get('disable_purge'));

    // Assert purge disabling.
    $this->purgeControl->disablePurge();
    $this->assertTrue($this->purgeSettings->get('disable_purge'));

    // Assert purge enabling.
    $this->purgeControl->enablePurge();
    $this->assertFalse($this->purgeSettings->get('disable_purge'));
  }

  /**
   * Test enabling/disabling automation.
   */
  public function testAutomation() {
    // By default purge is automatic.
    $this->assertTrue($this->purgeControl->isPurgeAutomated());

    // Purge is not automatic after switching it off.
    $this->purgeControl->setAutomation(FALSE);
    $this->assertFalse($this->purgeControl->isPurgeAutomated());

    $this->purgeControl->setAutomation(TRUE);
    $this->assertTrue($this->purgeControl->isPurgeAutomated());
  }

  /**
   * Test autoDisablePurge method.
   *
   * @covers ::autoDisablePurge
   */
  public function testAutoDisablePurge() {
    // Purge won't be disabled if auto control is disabled.
    $this->purgeSettings->set('purge_auto_control', FALSE)->save();
    $this->purgeControl->autoDisablePurge();
    $this->assertFalse($this->purgeSettings->get('disable_purge'));

    // Purge will be disabled if auto control is enabled.
    $this->purgeSettings->set('purge_auto_control', TRUE)->save();
    $this->purgeControl->autoDisablePurge();
    $this->assertTrue($this->purgeSettings->get('disable_purge'));
  }

  /**
   * Test autoEnablePurge method.
   *
   * @covers ::autoEnablePurge
   */
  public function testAutoEnablePurge() {
    $this->purgeControl->disablePurge();

    // Purge won't be enabled if auto control is disabled.
    $this->purgeSettings->set('purge_auto_control', FALSE)->save();
    $this->purgeControl->autoEnablePurge();
    $this->assertTrue($this->purgeSettings->get('disable_purge'));

    // Purge will be enabled if auto control is enabled.
    $this->purgeSettings->set('purge_auto_control', TRUE)->save();
    $this->purgeControl->autoEnablePurge();
    $this->assertFalse($this->purgeSettings->get('disable_purge'));
  }

}
