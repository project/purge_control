<?php

namespace Drupal\Tests\purge_control\Kernel\DiagnosticCheck;

use Drupal\purge\Plugin\Purge\DiagnosticCheck\PluginManager;
use Drupal\purge\Plugin\Purge\DiagnosticCheck\DiagnosticCheckInterface;
use Drupal\Tests\purge\Kernel\KernelPluginManagerTestBase;

/**
 * Tests "Purge enabled" diagnostic check plugin.
 *
 * @coversDefaultClass \Drupal\purge_control\Plugin\Purge\DiagnosticCheck\PurgeEnabledCheck
 *
 * @group purge_control
 */
class PurgeEnabledCheckTest extends KernelPluginManagerTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['purge_control'];

  /**
   * {@inheritdoc}
   */
  protected $pluginManagerClass = PluginManager::class;

  /**
   * {@inheritdoc}
   */
  public function setUp($switch_to_memory_queue = TRUE): void {
    parent::setUp($switch_to_memory_queue);

    $this->installConfig(['purge_control']);
  }

  /**
   * Test the plugin definition.
   */
  public function testDefinition(): void {
    // All metadata from \Drupal\purge\Annotation\PurgeDiagnosticCheck.
    $annotationFields = [
      'class',
      'dependent_purger_plugins',
      'dependent_queue_plugins',
      'description',
      'id',
      'provider',
      'title',
    ];

    // Plugin definition exists.
    $definitions = $this->pluginManager->getDefinitions();
    $this->assertArrayHasKey('purge_enabled', $definitions);

    // Annotation fields exist and only those that are needed.
    $definition_fields = array_keys($definitions['purge_enabled']);
    sort($definition_fields);
    $this->assertEquals($annotationFields, $definition_fields);

    // Assert definition values.
    $this->assertEquals('Purge enabled.', $definitions['purge_enabled']['title']);
    $this->assertEquals('Checks to see if the puring is enabled.', $definitions['purge_enabled']['description']);
    $this->assertEmpty($definitions['purge_enabled']['dependent_queue_plugins']);
    $this->assertEmpty($definitions['purge_enabled']['dependent_purger_plugins']);
  }

  /**
   * Test the plugin logic.
   *
   * @covers ::run
   */
  public function testRun() {
    // By default purging is enabled.
    $plugin = $this->pluginManager->createInstance('purge_enabled');
    $this->assertEquals(DiagnosticCheckInterface::SEVERITY_OK, $plugin->run());
    $this->assertEquals('Purging is enabled.', $plugin->getRecommendation());

    // Purging is disabled.
    $this->config('purge_control.settings')
      ->set('disable_purge', TRUE)
      ->save();
    $plugin = $this->pluginManager->createInstance('purge_enabled');
    $this->assertEquals(DiagnosticCheckInterface::SEVERITY_ERROR, $plugin->run());
    $this->assertEquals('Purging is disabled.', $plugin->getRecommendation());
  }

}
