<?php

namespace Drupal\Tests\purge_control\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\Traits\Core\CronRunTrait;

/**
 * Tests cron execution for purge_control.
 *
 * @group purge_control
 */
class PurgeControlCronTest extends BrowserTestBase {

  use CronRunTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['purge_control'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Purge control service instance.
   *
   * @var \Drupal\purge_control\Services\PurgeControl
   */
  protected $purgeControl;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->purgeControl = \Drupal::service('purge_control.purge_control');
  }

  /**
   * Test purge_control_cron function.
   */
  public function testCron() {
    // Disabled purge won't be enabled if automation is disabled.
    $this->purgeControl->disablePurge();
    $this->purgeControl->setAutomation(FALSE);
    $this->cronRun();
    $this->assertTrue($this->config('purge_control.settings')->get('disable_purge'));

    // Disabled purge will be enabled if automation is enabled.
    $this->purgeControl->setAutomation(TRUE);
    $this->cronRun();
    $this->assertFalse($this->config('purge_control.settings')->get('disable_purge'));
  }

}
