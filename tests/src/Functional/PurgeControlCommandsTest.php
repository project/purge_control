<?php

namespace Drupal\Tests\purge_control\Functional;

use Drupal\Tests\BrowserTestBase;
use Drush\TestTraits\DrushTestTrait;

/**
 * Tests drush commands.
 *
 * @coversDefaultClass \Drupal\purge_control\Drush\Commands\PurgeControlCommands
 *
 * @group purge_control
 */
class PurgeControlCommandsTest extends BrowserTestBase {

  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['purge_control'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test "purge-control" drush command.
   *
   * @covers ::purgeControl
   */
  public function testPurgeControl() {
    // Assert purge disabling.
    $this->drush('purge-control', ['disable-purge']);
    $this->assertStringContainsString('Purging is disabled.', $this->getErrorOutput());
    $this->refreshVariables();
    $this->assertTrue(\Drupal::config('purge_control.settings')->get('disable_purge'));

    // Assert purge enabling.
    $this->drush('purge-control', ['enable-purge']);
    $this->assertStringContainsString('Purging is enabled.', $this->getErrorOutput());
    $this->refreshVariables();
    $this->assertFalse(\Drupal::config('purge_control.settings')->get('disable_purge'));

    // Assert disabling of automation.
    $this->drush('purge-control', ['disable-automation']);
    $this->assertStringContainsString('Automated purge control is disabled.', $this->getErrorOutput());
    $this->refreshVariables();
    $this->assertFalse(\Drupal::config('purge_control.settings')->get('purge_auto_control'));

    // Assert enabling of automation.
    $this->drush('purge-control', ['enable-automation']);
    $this->assertStringContainsString('Automated purge control is enabled.', $this->getErrorOutput());
    $this->refreshVariables();
    $this->assertTrue(\Drupal::config('purge_control.settings')->get('purge_auto_control'));

    // Assert wrong option.
    $this->drush('purge-control', ['wrong-parameter']);
    $this->assertStringContainsString('Invalid operation.', $this->getErrorOutput());
  }

}
