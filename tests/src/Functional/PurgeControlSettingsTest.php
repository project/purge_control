<?php

namespace Drupal\Tests\purge_control\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests configuration form.
 *
 * @coversDefaultClass \Drupal\purge_control\PurgeControlSettings
 *
 * @group purge_control
 */
class PurgeControlSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'purge_control',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the settings form.
   */
  public function testSettingsForm() {
    // Anonymous user doesn't have access to the configuration form.
    $this->drupalGet('admin/config/development/performance/purge/purge-control');
    $this->assertSession()->statusCodeEquals(403);

    // Authenticated user doesn't have access to the configuration form.
    $authenticated_user = $this->drupalCreateUser();
    $this->drupalLogin($authenticated_user);
    $this->drupalGet('admin/config/development/performance/purge/purge-control');
    $this->assertSession()->statusCodeEquals(403);

    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/development/performance/purge/purge-control');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Purge Control Settings');

    // Assert default settings.
    $this->assertSession()->checkboxChecked('purge_auto_control');
    $this->assertSession()->checkboxNotChecked('disable_purge');

    $config = \Drupal::config('purge_control.settings');
    $this->assertTrue($config->get('purge_auto_control'));
    $this->assertFalse($config->get('disable_purge'));

    // Assert update of configuration.
    $edit = [
      'purge_auto_control' => FALSE,
      'disable_purge'  => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');

    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->checkboxNotChecked('purge_auto_control');
    $this->assertSession()->checkboxChecked('disable_purge');

    $config = \Drupal::config('purge_control.settings');
    $this->assertFalse($config->get('purge_auto_control'));
    $this->assertTrue($config->get('disable_purge'));
  }

}
