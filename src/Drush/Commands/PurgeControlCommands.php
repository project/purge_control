<?php

namespace Drupal\purge_control\Drush\Commands;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\purge_control\Services\PurgeControl;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Defines Drush commands for the module.
 */
final class PurgeControlCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * Constructs the PurgeControlCommands.
   *
   * @param \Drupal\purge_control\Services\PurgeControl $purgeControl
   *   PurgeControl service.
   */
  public function __construct(
    #[Autowire(service: 'purge_control.purge_control')]
    protected PurgeControl $purgeControl,
  ) {
    parent::__construct();
  }

  /**
   * Controls the enabling and disabling of purge on the site.
   */
  #[CLI\Command(name: 'purge-control', aliases: ['pc'])]
  #[CLI\Argument(name: 'op', description: 'Operation to perform.')]
  #[CLI\Usage(name: 'purge-control enable-purge', description: 'Enables purging.')]
  #[CLI\Usage(name: 'purge-control disable-purge', description: 'Disables purging.')]
  #[CLI\Usage(name: 'purge-control enable-automation', description: 'Enables automated purge control.')]
  #[CLI\Usage(name: 'purge-control disable-automation', description: 'Disables automated purge control.')]
  public function purgeControl(string $op): void {
    if ($op == 'enable-purge' || $op == 'enp') {
      $this->purgeControl->enablePurge();
      $this->logger()->notice(dt('Purging is enabled.'));
    }
    elseif ($op == 'disable-purge' || $op == 'disp') {
      $this->purgeControl->disablePurge();
      $this->logger()->notice(dt('Purging is disabled.'));
    }
    elseif ($op == 'enable-automation' || $op == 'ena') {
      $this->purgeControl->setAutomation(TRUE);
      $this->logger()->notice(dt('Automated purge control is enabled.'));
    }
    elseif ($op == 'disable-automation' || $op == 'disa') {
      $this->purgeControl->setAutomation(FALSE);
      $this->logger()->notice(dt('Automated purge control is disabled.'));
    }
    else {
      $this->logger()->error(dt('Invalid operation.'));
    }
  }

}
